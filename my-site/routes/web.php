<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('form'); //mise en place de l'accueil, un controlleur n'est pas obligatoire
});

/*
Documentation pour récuperer les données issuers d'une requête:
https://laravel.com/docs/5.8/requests#retrieving-input
*/